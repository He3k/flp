read_data(File):-repeat,
    read_line_to_codes(File, L),
    (L == 'end_of_file' -> seen,told,true; string_to_list(String, L),
    atomic_list_concat(Out, " ", String),
    tell('output.txt'), counter(Out), write('\n'),
    fail).

goal():-
    see('Input.txt'),
    seeing(File),
    read_data(File).

counter([]).
counter([Head|Tail]):-
    string_length(Head, N),
    (N == " " -> counter(Tail); write(Head),write('*'),counter(Tail)).