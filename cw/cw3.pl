:- consult('reader.txt').

% Предикат для просмотра содержимого базы данных
view_database :- listing(mark/3), fail.

% Предикат для добавления записей в базу данных
add_record :-
    write('student_name: '),
    read(Student),
    write('lesson_name: '),
    read(Subject),
    write('student_mark: '),
    read(Mark),
    assertz(mark(Student, Subject, Mark)),
    write('record_was_added.'),
    fail.

% Предикат для удаления записей из базы данных
delete_record :-
    write('student_name: '),
    read(Student),
    write('lesson_name: '),
    read(Subject),
    retract(mark(Student, Subject, _)),
    write('record_was_delete.'),
    fail.

% Предикат для выполнения запроса к базе данных
query_database :-
    findall(Student, (
        mark(Student, math, Mark1), Mark1 >= 4,
        mark(Student, history, Mark2), Mark2 >= 4,
        mark(Student, philosophy, Mark3), Mark3 >= 4,
        mark(Student, programming, Mark4), Mark4 > 3,
        mark(Student, english, Mark5), Mark5 > 3
    ), StudentList),
    write('student_with_increased_scholarships.'),
    write(StudentList),
    fail.

% Предикат для сохранения базы данных в файл
save_database :-
    tell('reader.txt'),
    listing(mark/3),
    told,
    write('database_was_saved_reader.txt').

% Главный предикат для работы с программой
main_menu :- repeat,
    nl, write('Menu:'),
    nl, write('1. show_database'),
    nl, write('2. add_database'),
    nl, write('3. delete_database'),
    nl, write('4. request_database'),
    nl, write('5. exit_with_save_database'),
    nl, read(Choice),
    ( Choice = 1 -> view_database
    ; Choice = 2 -> add_record
    ; Choice = 3 -> delete_record
    ; Choice = 4 -> query_database
    ; Choice = 5 -> save_database, halt
    ; write('enter_error_try_again'), fail
    ).